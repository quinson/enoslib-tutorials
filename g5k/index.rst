Jupyter + EnOSlib + Grid'5000 = 💖
===================================


.. toctree::
    :maxdepth: 1
    :caption: Contents:

    00_setup_and_basics.ipynb
    01_remote_actions_and_variables.ipynb
    02_observability.ipynb
    03_using_several_networks.ipynb
    04_working_with_virtualized_resources.ipynb
    05_network_emulation.ipynb
    06_orchestrators.ipynb